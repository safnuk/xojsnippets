#!/usr/bin/env python3

import gzip
import subprocess
import sys
import xml.etree.ElementTree as ET


class XojSnippets:

    def __init__(self, filename):
        self.snippets = []
        file = gzip.open(filename, 'rt')
        tree = ET.parse(file)
        root = tree.getroot()
        for page in root.findall('page'):
            for layer in page.findall('layer'):
                strokes = [Stroke(s) for s in layer.findall('stroke')]
                strings = layer.findall('text')
                if strings:
                    heights = [float(s.attrib['y']) for s in strings]
                    strings_with_heights = list(zip(heights, strings))
                    strings_with_heights.sort()
                    heights, strings = zip(*strings_with_heights)
                    heights = list(heights)
                    heights.append(float("inf"))
                    limits = zip(heights[:-1], heights[1:])
                    for text, (min, max) in zip(strings, limits):
                        self.snippets.append(
                            Snippet(text.text, min, max, strokes))


class Snippet:

    def __init__(self, label, min_y, max_y, strokes):
        self.label = label
        self.strokes = []
        for stroke in strokes:
            if min_y < stroke.height < max_y:
                self.strokes.append(stroke)

    def write_svg(self, file_prefix='fig-'):
        tmp_filename = '/tmp/' + self.label + '.svg'
        filename = file_prefix + self.label + '.svg'
        convert_options = ['-trim', '-bordercolor', 'white',
                           '-border', '10x10', '+repage']
        ET.register_namespace("", "http://www.w3.org/2000/svg")
        root = ET.XML(
            ('<svg width="612pt" height="792pt" version="1.1" '
             'viewBox="0 0 612 792" '
             'xmlns="http://www.w3.org/2000/svg"></svg>'))
        for s in self.strokes:
            p = ET.Element("path")
            p.set('fill', 'none')
            p.set('stroke', s.color)
            p.set('stroke-width', s.stroke_width)
            p.set('d', stroke_to_path(s.coords_str))
            root.append(p)
        tree = ET.ElementTree(root)
        tree.write(tmp_filename)
        run_list = ['convert', tmp_filename] + convert_options
        run_list.append(filename)
        subprocess.call(run_list)


class Stroke:

    def __init__(self, stroke_tag):
        self.color = stroke_tag.attrib['color']
        self.stroke_width = stroke_tag.attrib['width']
        self.coords_str = stroke_tag.text
        raw_coords = [float(s) for s in stroke_tag.text.split()]
        self.coords = []
        for n, (x, y) in enumerate(zip(raw_coords[:-1], raw_coords[1:])):
            if n % 2 == 0:
                self.coords.append((x, y))
        self.height = self.coords[0][1]


def stroke_to_path(s):
    coords = s.strip().split()
    head = coords[:2]
    tail = coords[2:]
    joined = ['M'] + head + ['L'] + tail
    return ' '.join(joined)


def main():
    input = sys.argv[1]
    xoj = XojSnippets(input)
    for s in xoj.snippets:
        s.write_svg()
