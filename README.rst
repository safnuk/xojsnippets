xojsnippets
===========

Simple script to pull sketches from an xoj file and output as svg files. Sketches are delineated by text entries, with the name of the output files given by "fig-TextEntry.svg", where /TextEntry/ is the text at the top of the sketch snippet.

Also includes the script xoj-watch, which watches a directory for changes to any xoj files present, calling xojsnippet on the changed files. This script uses inotifywatch for linux and fswatch for OSX.
