from setuptools import find_packages, setup


def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='xojsnippets',
      version='1.0.0',
      description=('Convert text-delineated pieces '
                   'from an xoj document into svg.'),
      long_description=readme(),
      url='https://bitbucket.org/safnuk/xojsnippets',
      author='Brad Safnuk',
      author_email='safnuk@gmail.com',
      license='MIT',
      packages=find_packages(),
      include_package_data=True,
      scripts=['scripts/xoj-watch'],
      entry_points={
          'console_scripts': [
              'xojsnippets = xojsnippets.xojsnippets:main',
          ]
      },
      classifiers=[
          "Development Status :: 3 - Alpha",
          "Intended Audience :: Science/Research"
          "Programming Language :: Python",
          "Programming Language :: Python :: 3",
          "License :: OSI Approved :: MIT License",
          "Operating System :: OS Independent",
      ],
      zip_safe=False)
